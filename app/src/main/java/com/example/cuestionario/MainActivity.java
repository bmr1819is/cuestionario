package com.example.cuestionario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements OnCheckedChangeListener {

    private CheckBox checkColima;
    private ToggleButton tooVillahermosa;
    private Switch swToluca;
    private RadioButton RdGuadalajara, RdDurango, RdNuevoleon, RdTuxtlagutierrez, RdOaxaca, RdPachuca,
            RdTepic, RdPuebla, RdCuliacan, RdXalapa, RdMerida, RdZacatecas, RdHermosillo, RdCdvictoria,
            RdCampeche, RdAguascalientes, RdLapaz;
    private TextView TvTotal;
    private EditText EtNombre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkColima = findViewById(R.id.colima);
        tooVillahermosa = findViewById(R.id.villahermosa);
        swToluca = findViewById(R.id.toluca);
        RdGuadalajara = findViewById(R.id.guadalajara);
        RdDurango = findViewById(R.id.durango);
        RdNuevoleon = findViewById(R.id.nuevoleon);
        RdTuxtlagutierrez = findViewById(R.id.tuxtlagutierrez);
        RdOaxaca = findViewById(R.id.oaxaca);
        RdPachuca = findViewById(R.id.pachuca);
        RdTepic = findViewById(R.id.tepic);
        RdPuebla = findViewById(R.id.puebla);
        RdCuliacan = findViewById(R.id.culiacan);
        RdXalapa = findViewById(R.id.xalapa);
        RdMerida = findViewById(R.id.merida);
        RdZacatecas = findViewById(R.id.zacatecas);
        RdHermosillo = findViewById(R.id.hermosillo);
        RdCdvictoria = findViewById(R.id.cdvictoria);
        RdCampeche = findViewById(R.id.campeche);
        RdAguascalientes = findViewById(R.id.aguascalientes);
        RdLapaz = findViewById(R.id.lapaz);
        TvTotal = findViewById(R.id.txtTotal);
        EtNombre = findViewById(R.id.nombre);


        checkColima.setOnCheckedChangeListener(this);
        tooVillahermosa.setOnCheckedChangeListener(this);
        swToluca.setOnCheckedChangeListener(this);
        RdGuadalajara.setOnCheckedChangeListener(this);
        RdDurango.setOnCheckedChangeListener(this);
        RdNuevoleon.setOnCheckedChangeListener(this);
        RdTuxtlagutierrez.setOnCheckedChangeListener(this);
        RdOaxaca.setOnCheckedChangeListener(this);
        RdPachuca.setOnCheckedChangeListener(this);
        RdTepic.setOnCheckedChangeListener(this);
        RdPuebla.setOnCheckedChangeListener(this);
        RdCuliacan.setOnCheckedChangeListener(this);
        RdXalapa.setOnCheckedChangeListener(this);
        RdMerida.setOnCheckedChangeListener(this);
        RdZacatecas.setOnCheckedChangeListener(this);
        RdHermosillo.setOnCheckedChangeListener(this);
        RdCdvictoria.setOnCheckedChangeListener(this);
        RdCampeche.setOnCheckedChangeListener(this);
        RdAguascalientes.setOnCheckedChangeListener(this);
        RdLapaz.setOnCheckedChangeListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int var = 0, suma = 0;
        String total;

        String minombre;
        minombre = EtNombre.getText().toString();

        var += checkColima.isChecked() ? 1 : 0;
        var += tooVillahermosa.isChecked() ? 1 : 0;
        var += swToluca.isChecked() ? 1 : 0;
        var += RdGuadalajara.isChecked() ? 1 : 0;
        var += RdDurango.isChecked() ? 1 : 0;
        var += RdNuevoleon.isChecked() ? 1 : 0;
        var += RdTuxtlagutierrez.isChecked() ? 1 : 0;
        var += RdOaxaca.isChecked() ? 1 : 0;
        var += RdPachuca.isChecked() ? 1 : 0;
        var += RdTepic.isChecked() ? 1 : 0;
        var += RdPuebla.isChecked() ? 1 : 0;
        var += RdCuliacan.isChecked() ? 1 : 0;
        var += RdXalapa.isChecked() ? 1 : 0;
        var += RdMerida.isChecked() ? 1 : 0;
        var += RdZacatecas.isChecked() ? 1 : 0;
        var += RdHermosillo.isChecked() ? 1 : 0;
        var += RdCdvictoria.isChecked() ? 1 : 0;
        var += RdCampeche.isChecked() ? 1 : 0;
        var += RdAguascalientes.isChecked() ? 1 : 0;
        var += RdLapaz.isChecked() ? 1 : 0;
        suma = suma + var;
        total = String.valueOf(suma);

        TvTotal.setText(minombre + " tus respuestas correctas: " + total);
    }
    public void calcular(View view) {


        TvTotal.setVisibility(android.view.View.VISIBLE);
    }
}
